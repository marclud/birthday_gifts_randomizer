import pytest

from models.birthday_lists import BirthdayLists

## Test data
list_of_people = [
    ["person_1", "test@", "person_2"],
    ["person_2", "test@", "person_1"],
    ["person_3", "test@", None],
    ["person_4", "test@", "person_5"],
    ["person_5", "test@", "person_4"]
]


def test_length_of_givers_and_receivers_list(new_list):
    list_of_people_length = len(list_of_people)
    givers_and_receivers_list_length = len(new_list.pairs_of_givers_and_receivers_list)
    assert list_of_people_length == givers_and_receivers_list_length


def test_validity_check_of_pairs_drew(new_list):
    for pair in new_list.pairs_of_givers_and_receivers_list:
        giver = pair[0]
        receiver = pair[1]
    assert giver["name"] != receiver["name"] and giver["name"] != receiver["relationship"]


@pytest.fixture
def new_list():
    test_list = BirthdayLists()
    test_list.creating_givers_and_receivers_lists(list_of_people)
    test_list.drawing_givers_and_receivers_pairs()
    return test_list
