import pytest

from models.birthday_lists import BirthdayLists

## Test data
list_of_people = [
    ["person_1", "test@", "person_2"],
    ["person_2", "test@", "person_1"],
    ["person_3", "test@", None],
    ["person_4", "test@", "person_5"],
    ["person_5", "test@", "person_4"]
]


## comparison if givers' names are the same as in test data
def test_givers_names(new_list):
    giver_number_on_the_list = 0
    for person in list_of_people:
        assert person[0] == new_list.givers_list[giver_number_on_the_list]["name"]
        giver_number_on_the_list += 1


## comparison if givers' emails are the same as in test data
def test_givers_emails(new_list):
    giver_number_on_the_list = 0
    for person in list_of_people:
        assert person[1] == new_list.givers_list[giver_number_on_the_list]["email"]
        giver_number_on_the_list += 1


## comparison if givers' relationship statuses are the same as in test data
def test_givers_relationships(new_list):
    giver_number_on_the_list = 0
    for person in list_of_people:
        assert person[2] == new_list.givers_list[giver_number_on_the_list]["relationship"]
        giver_number_on_the_list += 1


## comparison if receivers' names are the same as in test data
def test_receivers_names(new_list):
    receiver_number_on_the_list = 0
    for person in list_of_people:
        assert person[0] == new_list.receivers_list[receiver_number_on_the_list]["name"]
        receiver_number_on_the_list += 1


## comparison if receivers' emails are the same as in test data
def test_receivers_emails(new_list):
    receiver_number_on_the_list = 0
    for person in list_of_people:
        assert person[1] == new_list.receivers_list[receiver_number_on_the_list]["email"]
        receiver_number_on_the_list += 1


## comparison if receivers' relationship statuses are the same as in test data
def test_receivers_relationship(new_list):
    receiver_number_on_the_list = 0
    for person in list_of_people:
        assert person[2] == new_list.receivers_list[receiver_number_on_the_list]["relationship"]
        receiver_number_on_the_list += 1


@pytest.fixture
def new_list():
    test_list = BirthdayLists()
    test_list.creating_givers_and_receivers_lists(list_of_people)
    return test_list
