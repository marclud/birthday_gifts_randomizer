import smtplib
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class EmailModule:
    def __init__(self, sender_email, sending_list, subject):
        self.sender_email = sender_email
        self.sending_list = sending_list
        self.subject = subject

    def sending_email(self):
        for person in self.sending_list:
            receiver_name = person["giver_name"]
            receiver_email = person["giver_email"]
            surprise_person = person["receiver_name"]

            message = MIMEMultipart("alternative")
            message["Subject"] = self.subject
            message["From"] = self.sender_email
            message["To"] = receiver_email

            # Create the plain-text and HTML version of your message
            text = f"""\
            Cześć {receiver_name},
            w tym roku dajesz prezent {surprise_person}."""
            html = f"""\
            <html>
              <body>
                <p>Cześć {receiver_name},<br>
                   w tym roku dajesz prezent {surprise_person}.
                </p>
              </body>
            </html>
            """

            # Turn these into plain/html MIMEText objects
            part1 = MIMEText(text, "plain")
            part2 = MIMEText(html, "html")

            # Add HTML/plain-text parts to MIMEMultipart message
            # The email client will try to render the last part first
            message.attach(part1)
            message.attach(part2)

            # Create secure connection with server and send email
            print(f"Sending email to {receiver_name}")
            password = input("Type your password and press enter:")
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
                server.login(self.sender_email, password)
                server.sendmail(
                    self.sender_email, receiver_email, message.as_string()
                )
