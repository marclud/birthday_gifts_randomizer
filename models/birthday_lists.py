import random


class BirthdayLists:
    def __init__(self):
        self.givers_list = []
        self.receivers_list = []
        self.pairs_of_givers_and_receivers_list = []
        self.sending_list = []
        self.person_to_add = {}

    def creating_givers_and_receivers_lists(self, list):
        for person in list:
            self.person_to_add["name"] = person[0]
            self.person_to_add["email"] = person[1]
            self.person_to_add["relationship"] = person[2]
            self.givers_list.append(self.person_to_add)
            self.receivers_list.append(self.person_to_add)
            self.person_to_add = {}
        return self.givers_list, self.receivers_list

    def drawing_givers_and_receivers_pairs(self):
        is_randomizing_true = False
        pair_number_on_the_list = 0
        while not is_randomizing_true:
            random.shuffle(self.receivers_list)
            for giver in self.givers_list:
                receiver = self.receivers_list[0]
                self.receivers_list.remove(receiver)
                self.pairs_of_givers_and_receivers_list.append([giver, receiver])
            for giver in self.givers_list:
                receiver = self.pairs_of_givers_and_receivers_list[pair_number_on_the_list][1]
                if giver["name"] != receiver["name"] and giver["name"] != receiver["relationship"]:
                    is_randomizing_true = True
                    pair_number_on_the_list += 1
                else:
                    self.receivers_list += self.givers_list
                    self.pairs_of_givers_and_receivers_list = []
                    is_randomizing_true = False
                    pair_number_on_the_list = 0
                    break

    def creating_sending_list(self):
        for pair in self.pairs_of_givers_and_receivers_list:
            self.person_to_add["giver_name"] = pair[0]["name"]
            self.person_to_add["giver_email"] = pair[0]["email"]
            self.person_to_add["receiver_name"] = pair[1]["name"]
            self.sending_list.append(self.person_to_add)
            self.person_to_add = {}
        return self.sending_list
