# Birthday gift randomizer

## A tool which helps in creating random pairs for people who would like to play a game simillar to "secret santa"
- Create a list of people.
- Generate random pairs.
- Send information about receivers to givers via email.
