from models.birthday_lists import BirthdayLists
from lists.list_of_people import test_group_of_people
from models.sending_emails_module import EmailModule

test_group = BirthdayLists()
test_group.creating_givers_and_receivers_lists(test_group_of_people)
test_group.drawing_givers_and_receivers_pairs()
test_group.creating_sending_list()

emailing = EmailModule("test_mail@", test_group.sending_list, "test")
emailing.sending_email()
